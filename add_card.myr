use std

use bio
use date
use sys

use t

use "util"

const main = {args
        var dir_arg : byte[:] = [][:]
        var a_file_arg : byte[:] = [][:]
        var b_file_arg : byte[:] = [][:]
        var a_string_arg : byte[:] = [][:]
        var b_string_arg : byte[:] = [][:]
        var name_arg : byte[:] = [][:]
        var basedir : byte[:] = [][:]
        var dir_checked = false
        var cmd : std.optparsed = std.optparse(args, &[
                .minargs = 0,
                .maxargs = 0,
                .opts = [
                        [ .opt = 'd', .arg = "dir", .desc = "base directory", .optional = true ],
                        [ .opt = 'n', .arg = "name", .desc = "card name (for uniqueness)", .optional = false ],
                        [ .opt = 'a', .arg = "a-file", .desc = "file for A-side" ],
                        [ .opt = 'A', .arg = "a-string", .desc = "string for A-side" ],
                        [ .opt = 'b', .arg = "b-file", .desc = "file for B-side" ],
                        [ .opt = 'B', .arg = "b-string", .desc = "string for B-side" ],
                ][:],
        ])

        for opt : cmd.opts
                match opt
                | ('d', dir): dir_arg = dir
                | ('n', name): name_arg = name
                | ('a', arg): a_file_arg = arg
                | ('b', arg): b_file_arg = arg
                | ('A', arg): a_string_arg = arg
                | ('B', arg): b_string_arg = arg
                | _: std.fatal("impossible {}\n", opt)
                ;;
        ;;

        match get_basedir(dir_arg)
        | `std.Err e: std.fatal("{}\n", e)
        | `std.Ok b: basedir = b
        ;;
        auto (basedir : t.doomed_str)

        if name_arg.len == 0
                std.fatal("-n is required\n")
        elif (a_file_arg.len == 0) && (a_string_arg.len == 0)
                std.fatal("One of -a or -A is required\n")
        elif (a_file_arg.len != 0) && (a_string_arg.len != 0)
                std.fatal("Only one of -a or -A is permitted\n")
        elif (b_file_arg.len == 0) && (b_string_arg.len == 0)
                std.fatal("One of -b or -B is required\n")
        elif (b_file_arg.len != 0) && (b_string_arg.len != 0)
                std.fatal("Only one of -b or -B is permitted\n")
        ;;

        match choose_arg(a_string_arg, a_file_arg)
        | `std.Ok str: a_string_arg = str
        | `std.Err e: std.fatal("{}\n", e)
        ;;

        match choose_arg(b_string_arg, b_file_arg)
        | `std.Ok str: b_string_arg = str
        | `std.Err e: std.fatal("{}\n", e)
        ;;

        var card_dir : byte[:] = std.pathjoin([basedir, "cards", name_arg][:])

        match std.diropen(card_dir)
        | `std.Ok d2:
                std.dirclose(d2)
                std.fatal("card \"{}\" already exists\n", name_arg)
        | `std.Err e:
        ;;

        match std.mkpath(card_dir)
        | std.Enone:
        | e: std.fatal("mkdir(\"{}\"): {}\n", card_dir, e)
        ;;

        var sched_dir_path = std.pathjoin([basedir, "schedule"][:])
        match std.mkpath(sched_dir_path)
        | std.Enone:
        | e: std.fatal("mkdir(\"{}\"): {}\n", sched_dir_path, e)
        ;;

        var a_path : byte[:] = std.pathjoin([card_dir, "side_A"][:])
        var b_path : byte[:] = std.pathjoin([card_dir, "side_B"][:])
        var level_path : byte[:] = std.pathjoin([card_dir, "level"][:])
        var next_review : date.instant = date.subperiod(date.utcnow(), `date.Day 7)
        var sched_path = std.pathjoin([sched_dir_path, std.fmt("{f=%Y-%m-%d}", next_review)][:])

        match std.open(sched_path, std.Owrite | std.Ocreat | std.Oappend)
        | `std.Err e: std.fput(std.Err, "std.open(\"{}\"): {}\n", sched_path, e)
        | `std.Ok fd:
                std.fput(fd, "{}\n", name_arg)
                std.close(fd)
        ;;

        match std.open(level_path, std.Owrite | std.Ocreat | std.Oappend)
        | `std.Err e: std.fput(std.Err, "std.open(\"{}\"): {}\n", level_path, e)
        | `std.Ok fd:
                std.fput(fd, "1\n")
                std.close(fd)
        ;;

        match std.open(a_path, std.Owrite | std.Ocreat | std.Oappend)
        | `std.Err e: std.fput(std.Err, "std.open(\"{}\"): {}\n", a_path, e)
        | `std.Ok fd:
                std.fput(fd, "{}", a_string_arg)
                std.close(fd)
        ;;

        match std.open(b_path, std.Owrite | std.Ocreat | std.Oappend)
        | `std.Err e: std.fput(std.Err, "std.open(\"{}\"): {}\n", b_path, e)
        | `std.Ok fd:
                std.fput(fd, "{}", b_string_arg)
                std.close(fd)
        ;;
}

const choose_arg = {str : byte[:], path : byte[:]
        if str.len > 0
                -> `std.Ok std.fmt("{}\n", str)
        ;;

        match std.slurp(path)
        | `std.Ok buf:
                if buf.len > 0
                        -> `std.Ok buf
                ;;

                -> `std.Err std.fmt("file \"{}\" seems empty", path)
        | `std.Err e:
                -> `std.Err std.fmt("std.slurp(\"{}\"): {}", path, e)
        ;;
}
