use std

use bio
use date
use escfmt
use json
use sys

use t

use "util"

const main = {args
        var dir_arg : byte[:] = [][:]
        var a_string : byte[:] = [][:]
        var b_string : byte[:] = [][:]
        var name : byte[:] = [][:]
        var basedir : byte[:] = [][:]
        var dir_checked = false
        var cmd : std.optparsed = std.optparse(args, &[
                .minargs = 1,
                .maxargs = 1,
                .opts = [
                        [ .opt = 'd', .arg = "dir", .desc = "base directory", .optional = true ],
                ][:],
        ])

        for opt : cmd.opts
                match opt
                | ('d', dir): dir_arg = dir
                | _: std.fatal("impossible {}\n", opt)
                ;;
        ;;

        match get_basedir(dir_arg)
        | `std.Err e: std.fatal("{}\n", e)
        | `std.Ok b: basedir = b
        ;;
        auto (basedir : t.doomed_str)

        if cmd.args.len != 1
                std.fatal("exactly one argument required\n")
        ;;

        match from_jisho(cmd.args[0])
        | `std.Ok (n, a, b): (name, a_string, b_string) = (n, a, b)
        | `std.Err e: std.fatal("{}\n", e)
        ;;
        var card_name : byte[:] = std.fmt("jisho_word_{}", name)
        auto (card_name : t.doomed_str)
        var card_dir : byte[:] = std.pathjoin([basedir, "cards", card_name][:])

        match std.diropen(card_dir)
        | `std.Ok d2:
                std.dirclose(d2)
                std.fatal("card \"{}\" already exists\n", name)
        | `std.Err e:
        ;;

        match std.mkpath(card_dir)
        | std.Enone:
        | e: std.fatal("mkdir(\"{}\"): {}\n", card_dir, e)
        ;;

        var sched_dir_path = std.pathjoin([basedir, "schedule"][:])
        match std.mkpath(sched_dir_path)
        | std.Enone:
        | e: std.fatal("mkdir(\"{}\"): {}\n", sched_dir_path, e)
        ;;

        var a_path : byte[:] = std.pathjoin([card_dir, "side_A"][:])
        var b_path : byte[:] = std.pathjoin([card_dir, "side_B"][:])
        var level_path : byte[:] = std.pathjoin([card_dir, "level"][:])
        var next_review : date.instant = date.subperiod(date.utcnow(), `date.Day 7)
        var sched_path = std.pathjoin([sched_dir_path, std.fmt("{f=%Y-%m-%d}", next_review)][:])

        match std.open(sched_path, std.Owrite | std.Ocreat | std.Oappend)
        | `std.Err e: std.fput(std.Err, "std.open(\"{}\"): {}\n", sched_path, e)
        | `std.Ok fd:
                std.fput(fd, "{}\n", card_name)
                std.close(fd)
        ;;

        match std.open(level_path, std.Owrite | std.Ocreat | std.Oappend)
        | `std.Err e: std.fput(std.Err, "std.open(\"{}\"): {}\n", level_path, e)
        | `std.Ok fd:
                std.fput(fd, "1\n")
                std.close(fd)
        ;;

        match std.open(a_path, std.Owrite | std.Ocreat | std.Oappend)
        | `std.Err e: std.fput(std.Err, "std.open(\"{}\"): {}\n", a_path, e)
        | `std.Ok fd:
                std.fput(fd, "{}\n", a_string)
                std.close(fd)
        ;;

        match std.open(b_path, std.Owrite | std.Ocreat | std.Oappend)
        | `std.Err e: std.fput(std.Err, "std.open(\"{}\"): {}\n", b_path, e)
        | `std.Ok fd:
                std.fput(fd, "{}\n", b_string)
                std.close(fd)
        ;;

        std.put("Added to {}\n", card_dir)
}

const from_jisho = { w : byte[:]
        var url = std.fmt("https://jisho.org/api/v1/search/words?keyword={}", escfmt.url(w))
        var human_url = std.fmt("https://jisho.org/word/{}", escfmt.url(w))
        var res : byte[:] = [][:]
        var root : json.elt#
        var found_anything : bool = false
        var name : byte[:] = [][:]
        var side_b : std.strbuf# = std.mksb()
        auto (url : t.doomed_str)
        auto (human_url : t.doomed_str)

        match std.spork(["curl", "-s", url][:])
        | `std.Err e: -> `std.Err std.fmt("spork(): {}", e)
        | `std.Ok (pid, infd, outfd):
                std.close(infd)
                match std.fslurp(outfd)
                | `std.Ok buf: res = buf
                | `std.Err e: -> `std.Err std.fmt("slurp(): {}", e)
                ;;

                match std.wait(pid)
                | `std.Waiterror: -> `std.Err "waitpid(curl): Waiterror"
                | `std.Wfailure: -> `std.Err "waitpid(curl): Wfailure"
                | `std.Wsignalled : -> `std.Err "waitpid(curl): Wsignalled"
                | `std.Wsuccess:
                ;;
        ;;

        match json.parse(res)
        | `std.Err e: -> `std.Err std.fmt("json.parse()", res)
        | `std.Ok r: root = r
        ;;

        match by_path(root, ["meta", "status"][:])
        | `std.Some v:
                match v#
                | `json.Num json_code:
                        if json_code != 200.0
                                -> `std.Err std.fmt("response code: {}", json_code)
                        ;;
                | _: -> `std.Err std.fmt("response {}", v)
                ;;
        | _: -> `std.Err "malformed json response (no status code)"
        ;;

        /* Now walk through the data and try to pick out parts that match */
        var data : json.elt#[:] = [][:]
        match by_path(root, ["data"][:])
        | `std.Some &(`json.Arr v): data = v
        | `std.Some v: -> `std.Err std.fmt("malformed json response (data was \"{}\")", v)
        | `std.None: -> `std.Err "malformed json response (no data)"
        ;;

        std.sbfmt(side_b, "{}\n\n", human_url)

        for var j = 0; j < data.len; ++j
                var srs = get_spelling_readings(data[j])
                for (spelling, reading) : srs
                        if std.eq(spelling, w) || std.eq(reading, w)
                                append_word_desc(side_b, data[j], srs)
                                break
                        ;;
                ;;
        ;;
        

        -> `std.Ok (w, w, std.sbfin(side_b))  
}

const by_path : (root : json.elt#, seq : byte[:][:] -> std.option(json.elt#)) = {root : json.elt#, seq : byte[:][:]
        while seq.len > 0
                match root#
                | `json.Obj a:
                        var success = false
                        for var j = 0; j < a.len; ++j
                                var key : byte[:] = a[j].0
                                var val : json.elt# = a[j].1
                                if std.eq(key, seq[0])
                                        root = val
                                        seq = seq[1:]
                                        success = true
                                        j = a.len
                                ;;
                        ;;

                        if !success
                                -> `std.None
                        ;;
                | _:
                        -> `std.None
                ;;
        ;;

        -> `std.Some root
}

/* Just take the first entry of "japanese" that has both a spelling and a reading */
const get_spelling_readings = {root : json.elt#
        var ret : (byte[:], byte[:])[:] = [][:]
        match by_path(root, ["japanese"][:])
        | `std.Some &(`json.Arr a):
                for var j = 0; j < a.len; ++j
                        match (by_path(a[j], ["word"][:]), by_path(a[j], ["reading"][:]))
                        | (`std.Some &(`json.Str w), `std.Some &(`json.Str r)):
                                std.slpush(&ret, (w, r))
                        | (`std.None, `std.Some &(`json.Str r)):
                                std.slpush(&ret, ("", r))
                        | _:
                        ;;
                ;;
        | _:
        ;;

        -> ret
}

const append_word_desc = {sb : std.strbuf#, root : json.elt#, srs : (byte[:], byte[:])[:]
        /*
             本 (ほん)

             Noun

               1. book; volume; script

             Prefix

               2. this; present

               3. main; head

               4. real; regular

             Suffix, Counter

               5. counter for long cylindrical things; counter for films, TV shows, etc.; counter for goals, home runs, etc.; counter for telephone calls​
         */
        var n = 1
        for (spelling, reading) : srs
                if spelling.len > 0
                        std.sbfmt(sb, "{}  ({})\n", spelling, reading)
                else
                        std.sbfmt(sb, "{}\n", reading)
                ;;
        ;;
        std.sbfmt(sb, "\n")

        match by_path(root, ["senses"][:])
        | `std.Some &(`json.Arr a):
                for var j = 0; j < a.len; ++j
                        match by_path(a[j], ["parts_of_speech"][:])
                        | `std.Some &(`json.Arr ap):
                                var first = true
                                for var k = 0; k < ap.len; ++k
                                        match ap[k]
                                        | &(`json.Str s):
                                                if !first
                                                        std.sbfmt(sb, ", ")
                                                ;;
                                                std.sbfmt(sb, "{}", s)
                                                first = false
                                        | _:
                                        ;;
                                ;;
                                if !first
                                        std.sbfmt(sb, "\n\n")
                                ;;
                        | _:
                        ;;

                        match by_path(a[j], ["english_definitions"][:])
                        | `std.Some &(`json.Arr ap):
                                var first = true
                                for var k = 0; k < ap.len; ++k
                                        match ap[k]
                                        | &(`json.Str s):
                                                if !first
                                                        std.sbfmt(sb, ", ")
                                                else
                                                        std.sbfmt(sb, " {w=3,p= }. ", n)
                                                ;;
                                                std.sbfmt(sb, "{}", s)
                                                first = false
                                        | _:
                                        ;;
                                ;;

                                if !first
                                        std.sbfmt(sb, "\n\n")
                                ;;

                                n++
                        | _:
                        ;;

                ;;
        | _:
        ;;
}
